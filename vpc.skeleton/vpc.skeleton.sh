#!/bin/bash
cfn-flip vpc.skeleton.template.yaml > vpc.skeleton.template.json
cfn-flip vpc.skeleton.param.yaml > vpc.skeleton.param.json
aws cloudformation --region ap-southeast-1 create-stack --stack-name vpc --template-body file://vpc.skeleton.template.json --parameters file://vpc.skeleton.param.json
