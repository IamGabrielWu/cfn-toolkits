#!/bin/bash
STACK_NAME="ssstack"
cfn-flip ssserver.tpl.yml > ssserver.tpl.json
cfn-flip ssserver.param.yml >ssserver.param.json
#below make sure the latest stack is create_complete and exists
STACKSTATE=$(aws cloudformation --region us-east-1  list-stacks --stack-status-filter  "CREATE_COMPLETE" | jq  '.StackSummaries|.[0]|.StackName')
echo $STACKSTATE
if [ ! -z "$STACKSTATE" -a "$STACKSTATE" != " " -a "$STACKSTATE" != "null" ]; then
  aws cloudformation --region us-east-1 update-stack --stack-name $STACK_NAME --capabilities CAPABILITY_NAMED_IAM --template-body file://ssserver.tpl.json --parameters file://ssserver.param.json
else
  aws cloudformation --region us-east-1 create-stack --stack-name $STACK_NAME --capabilities CAPABILITY_NAMED_IAM --template-body file://ssserver.tpl.json --parameters file://ssserver.param.json
fi
