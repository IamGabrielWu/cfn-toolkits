#!/bin/bash
cfn-flip kubernete-setup.yml > kubernete-setup.json
cfn-flip kubernete-setup.param.yml > kubernete-setup.param.json
aws cloudformation --region us-east-1 create-stack --capabilities CAPABILITY_NAMED_IAM --stack-name kubernetes --template-body file://kubernete-setup.json --parameters file://kubernete-setup.param.json
